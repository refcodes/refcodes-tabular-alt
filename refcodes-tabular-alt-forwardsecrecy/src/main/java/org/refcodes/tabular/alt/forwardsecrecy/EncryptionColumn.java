// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular.alt.forwardsecrecy;

import java.text.ParseException;

import org.refcodes.data.Text;
import org.refcodes.forwardsecrecy.EncryptionProvider;
import org.refcodes.security.EncryptionException;
import org.refcodes.security.EncryptionException.EncryptionRuntimeException;
import org.refcodes.tabular.AbstractColumn;
import org.refcodes.tabular.Column;

/**
 * This {@link Column} solely can encrypt {@link String} texts using the
 * refcodes-forwardsecrecy framework.
 */
public class EncryptionColumn extends AbstractColumn<String> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final EncryptionProvider _encryptionProvider;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Creates an {@link EncryptionColumn} encrypting any text.
	 *
	 * @param aKey The key to be used.
	 * @param aEncryptionProvider The {@link EncryptionProvider} to be used.
	 */
	public EncryptionColumn( String aKey, EncryptionProvider aEncryptionProvider ) {
		super( aKey, String.class );
		_encryptionProvider = aEncryptionProvider;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] toStorageStrings( String aValue ) {
		if ( aValue == null ) {
			return null;
		}
		try {
			return new String[] { _encryptionProvider.toEncrypted( aValue ) };
		}
		catch ( EncryptionException e ) {
			throw new EncryptionRuntimeException( e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String fromStorageStrings( String[] aStringValues ) throws ParseException {
		throw new UnsupportedOperationException( Text.UNSUPPORTED_OPERATION.getText() );
	}

	// /////////////////////////////////////////////////////////////////////////
	// COMMON:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
