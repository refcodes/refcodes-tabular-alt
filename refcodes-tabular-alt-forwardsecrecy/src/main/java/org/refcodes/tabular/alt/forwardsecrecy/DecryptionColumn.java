// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular.alt.forwardsecrecy;

import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.refcodes.data.Text;
import org.refcodes.forwardsecrecy.DecryptionProvider;
import org.refcodes.forwardsecrecy.NoCipherUidException;
import org.refcodes.forwardsecrecy.UnknownCipherUidException;
import org.refcodes.tabular.AbstractColumn;
import org.refcodes.tabular.Column;

/**
 * This {@link Column} solely can decrypt {@link String} texts using the
 * refcodes-forwardsecrecy framework.
 */
public class DecryptionColumn extends AbstractColumn<String> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final Logger LOGGER = Logger.getLogger( DecryptionColumn.class.getName() );

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final DecryptionProvider _decryptionProvider;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new decryption column impl.
	 *
	 * @param aKey the key
	 * @param aDecryptionProvider the decryption provider
	 */
	public DecryptionColumn( String aKey, DecryptionProvider aDecryptionProvider ) {
		super( aKey, String.class );
		_decryptionProvider = aDecryptionProvider;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] toStorageStrings( String aValue ) {
		throw new UnsupportedOperationException( Text.UNSUPPORTED_OPERATION.getText() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String fromStorageStrings( String[] aStringValues ) throws ParseException {
		if ( aStringValues == null || aStringValues.length == 0 ) {
			return null;
		}
		else if ( aStringValues.length == 1 ) {
			try {
				return _decryptionProvider.toDecrypted( aStringValues[0] );
			}
			catch ( UnknownCipherUidException e ) {
				LOGGER.log( Level.WARNING, "Encountered an unknown cipher UID \"" + e.getUniversalId() + "\", unable to decrypt (returning encrypted value) as of: " + e.toMessage(), e );
				return aStringValues[0];
			}
			catch ( NoCipherUidException e ) {
				LOGGER.log( Level.WARNING, "No cipher UID was found for decryption (returning encrypted value) as of: " + e.toMessage(), e );
				return aStringValues[0];
			}
		}
		throw new IllegalArgumentException( "The type <" + getType().getName() + "> is not an array type though the number of elements in the provided string array is <" + aStringValues.length + "> whereas only one element is being expected." );
	}

	// /////////////////////////////////////////////////////////////////////////
	// COMMON:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
