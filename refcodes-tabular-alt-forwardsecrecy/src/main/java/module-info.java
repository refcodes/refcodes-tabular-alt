module org.refcodes.tabular.alt.forwardsecrecy {
	requires org.refcodes.data;
	requires org.refcodes.security;
	requires transitive org.refcodes.exception;
	requires transitive org.refcodes.forwardsecrecy;
	requires transitive org.refcodes.tabular;
	requires java.logging;
	requires org.refcodes.runtime;

	exports org.refcodes.tabular.alt.forwardsecrecy;
}
