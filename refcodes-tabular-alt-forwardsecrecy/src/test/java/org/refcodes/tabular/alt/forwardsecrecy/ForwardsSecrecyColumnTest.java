// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular.alt.forwardsecrecy;

import static org.junit.jupiter.api.Assertions.*;
import java.text.ParseException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.refcodes.forwardsecrecy.DecryptionProvider;
import org.refcodes.forwardsecrecy.DecryptionProviderImpl;
import org.refcodes.forwardsecrecy.EncryptionProvider;
import org.refcodes.forwardsecrecy.EncryptionProviderImpl;
import org.refcodes.forwardsecrecy.EncryptionServer;
import org.refcodes.forwardsecrecy.EncryptionService;
import org.refcodes.forwardsecrecy.InMemoryDecryptionServer;
import org.refcodes.forwardsecrecy.InMemoryEncryptionServer;
import org.refcodes.forwardsecrecy.LoopbackDecryptionService;
import org.refcodes.forwardsecrecy.LoopbackEncryptionService;
import org.refcodes.runtime.SystemProperty;

@SuppressWarnings("deprecation")
public class ForwardsSecrecyColumnTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String NAMESPACE = "test";

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	EncryptionColumn _encryptionColumn = null;
	DecryptionColumn _decryptionColumn = null;

	// /////////////////////////////////////////////////////////////////////////
	// SETUP:
	// /////////////////////////////////////////////////////////////////////////

	@BeforeEach
	public void beforeEach() {
		final InMemoryDecryptionServer theDecryptionServer = new InMemoryDecryptionServer();
		final LoopbackDecryptionService theDecryptionService = new LoopbackDecryptionService( NAMESPACE, theDecryptionServer );
		theDecryptionService.setCipherVersionsExpireTimeMillis( 500 );
		final DecryptionProvider theDecryptionProvider = new DecryptionProviderImpl( theDecryptionService );
		final EncryptionServer theEncryptionServer = new InMemoryEncryptionServer( theDecryptionServer );
		final EncryptionService theEncryptionService = new LoopbackEncryptionService( NAMESPACE, theEncryptionServer );
		final EncryptionProvider theEncryptionProvider = new EncryptionProviderImpl( theEncryptionService );
		_encryptionColumn = new EncryptionColumn( "value", theEncryptionProvider );
		_decryptionColumn = new DecryptionColumn( "value", theDecryptionProvider );
	}

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testForwardsSecrecyColumn1() throws ParseException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "*** Testing conversion of a decrypted string value to an encrypted storage string and back ***".toUpperCase() );
		}
		final String theValue = "This is a test text.";
		final String theToStorageString = _encryptionColumn.toStorageString( theValue );
		final String theFromStorageString = _decryptionColumn.fromStorageString( theToStorageString );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Value --> To storage string --> From storage string := " + theValue + " --> " + theToStorageString + " --> " + theFromStorageString );
		}
		assertEquals( theValue, theFromStorageString );
	}

	@Test
	public void testForwardsSecrecyColumn2() throws ParseException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "*** Testing conversion of a decrypted string value to an encrypted storage string array and back ***".toUpperCase() );
		}
		final String theValue = "This is a test text.";
		final String[] theToStorageStrings = _encryptionColumn.toStorageStrings( theValue );
		final String theFromStorageString = _decryptionColumn.fromStorageStrings( theToStorageStrings );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Value --> To storage string --> From storage string := " + theValue + " --> " + theToStorageStrings + " --> " + theFromStorageString );
		}
		assertEquals( theValue, theFromStorageString );
	}
}
